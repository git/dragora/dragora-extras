# Build recipe for adwaita-icon-theme.
#
# Copyright (c) 2018-2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=adwaita-icon-theme
version=3.34.0
arch=noarch
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/x-libs"

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch="https://ftp.gnome.org/pub/gnome/sources/adwaita-icon-theme/${version%.*}/$tarname"

description="
Icon theme for GTK3+ applications.

The adwaita-icon-theme package contains an icon theme for GTK+3
applications.
"

homepage=https://www.gnome.org
license="LGPLv3+, CC-BY-SA 3.0"

# Source documentation
docs="AUTHORS COPYING* NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure $configure_args --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
    chmod 644 "${destdir}/${docsdir}"/*
}

