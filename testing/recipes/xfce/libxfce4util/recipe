# Build recipe for libxfce4util.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018-2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=libxfce4util
version=4.14.0
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xfce"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch="https://archive.xfce.org/src/xfce/libxfce4util/${version%.*}/$tarname"

description="
A basic utility library for Xfce.

This is a general-purpose utility library used by Xfce.
"

homepage=https://xfce.org
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING NEWS README THANKS TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
    $configure_args \
    --libdir=/usr/lib${libSuffix} \
    --enable-static=no \
    --enable-shared=yes \
    --enable-debug=no \
    --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

