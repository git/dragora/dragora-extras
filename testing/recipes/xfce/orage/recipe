# Build recipe for orage.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=orage
version=4.12.1
release=2

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xfce"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://archive.xfce.org/src/apps/orage/4.12/$tarname

description="
A simple calendar application with reminders.

This is mainly used on the Xfce environment.
"

homepage=https://xfce.org/projects/
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING NEWS README TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --enable-static=no \
     --enable-shared=yes \
     --enable-debug=no \
     --enable-libxfce4panel=yes \
     --enable-libical=no \
     --enable-dbus=yes \
     --enable-libnotify=yes \
     --enable-archive=yes \
     --enable-reentrant \
     --enable-final \
     --with-locales-dir=/usr/lib${libSuffix}/locale \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Move locale dir to the corresponding location

    if test -d "${destdir}/usr/lib/locale"
    then
        mkdir -p "${destdir}/usr/lib${libSuffix}"
        mv "${destdir}/usr/lib/locale" "${destdir}/usr/lib${libSuffix}"
    fi
    rmdir "${destdir}/usr/lib"

    lzip -9 "${destdir}/${mandir}"/man?/*.?

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

